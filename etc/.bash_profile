# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/bin:$HOME/.phpbrew/bin
PATH=$PATH:/var/www/mst/vendor/bin

export PATH

export EDITOR=vi
source ~/.phpbrew/bashrc
phpbrew fpm restart
