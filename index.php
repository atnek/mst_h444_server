<?php

require __DIR__ . '/vendor/autoload.php';

use Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider,
    Doctrine\DBAL\Configuration,
    MST\Config,
    MST\Provider\AccountProvider,
    Igorw\Silex\ConfigServiceProvider,
    Predis\Silex\PredisServiceProvider,
    Silex\Provider\DoctrineServiceProvider,
    Silex\Provider\FormServiceProvider,
    Silex\Provider\MonologServiceProvider,
    Silex\Provider\ServiceControllerServiceProvider,
    Silex\Provider\SessionServiceProvider,
    Silex\Provider\SecurityServiceProvider,
    Silex\Provider\TranslationServiceProvider,
    Silex\Provider\TwigServiceProvider,
    Silex\Provider\ValidatorServiceProvider;

// セットアップ
$app = \MST\App::app();
$app['debug'] = Config::DEBUG;

// 認証
$app->register(new SecurityServiceProvider(), array(
    'security.firewalls' => [
        'unsecure' => [
            'pattern' => '^/',
            'anonymous' => true,
        ],
        'auth' => [
            'pattern' => '/mst/api/',
            'http' => true, // TODO httpsだけ許可する
            'security' => true,
            'users' => $app->share(function () use ($app) {
                    return new AccountProvider();
                }),
        ],
    ]
));

// redis
$app->register(new PredisServiceProvider(), ['predis.parameters' => Config::Redis_Host]);

// セッション
$app->register(new SessionServiceProvider());
// セッションの保持にredisを使うため
$app['session.storage.handler'] = null;

// ログ
$logDir = '/var/log/mst';
$app->register(new MonologServiceProvider(), ['monolog.logfile' => $logDir . '/development.log']);

// DB
$app->register(
    new DoctrineServiceProvider(), [
    'db.options' => Config::$DBParams,
    'db.config' => new Configuration([
        \PDO::ATTR_AUTOCOMMIT => Config::DB_UseAutoCommit,
        \PDO::ATTR_EMULATE_PREPARES => !Config::DB_UseStaticPlaceHolder,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
        ]),
]);

$app->register(
    new DoctrineOrmServiceProvider(), [
    'orm.proxies_dir' => __DIR__ . '/cache/db_proxy/',
    'orm.em.options' => array(
        'mappings' => array(
            array(
                'type' => 'annotation',
                'namespace' => 'MST\Entity',
                'path' => 'src/MST/Entity',
                'use_simple_annotation_reader' => false,
            ),
        ),
    ),
]);

// テンプレートエンジン
$app->register(new TwigServiceProvider(), ['twig.path' => __DIR__ . '/views']);
$app->register(new FormServiceProvider());
$app->register(new ValidatorServiceProvider());
$app->register(new TranslationServiceProvider());

// コントローラ
$app->register(new ServiceControllerServiceProvider());
$app->register(new ConfigServiceProvider(__DIR__ . '/routes.yml'));

// 以下ルーティング
foreach ($app['config.routes'] as $name => $route) {
    $app->match($route['pattern'], $route['route'])->method($route['method'])->bind($name);
}

// コントローラ登録
foreach (Config::$Controllers as $controller) {
    $ns = explode('\\', $controller);
    $app['controllers.' . end($ns)] = new $controller($app);
}

// ルーティング開始
$app->run();

