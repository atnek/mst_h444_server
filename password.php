<?php
require_once __DIR__.'/vendor/autoload.php';

use Silex\Application;
use Symfony\Component\Security\Core\User\User;

$app = new Application();
$app->register(new Silex\Provider\SecurityServiceProvider());
$app['security.firewalls'] = [];

$username = 'root';
$password = 'pass';

$user = new User($username, $password);
$encoder = $app['security.encoder_factory']->getEncoder($user);
echo $encoder->encodePassword($password, $user->getSalt());
