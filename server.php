<?php

require_once __DIR__ . '/vendor/autoload.php';

use MST\Controller\WebSocket\WebSocketController;

$settings = [
    'port' => 9001,
    'host' => 'localhost',
];

echo 'starting server...' . PHP_EOL;

$ws = new \Ratchet\WebSocket\WsServer(new WebSocketController());
$server = Ratchet\Server\IoServer::factory(
        new \Ratchet\Http\HttpServer($ws), $settings['port'], $settings['host']);

echo $settings['host'] . ':' . $settings['port'] . '@websocketserver running' . PHP_EOL;

$server->run();
