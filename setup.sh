#!/bin/sh
cat composer.lock && composer update
cat composer.lock || composer install
mkdir /var/log/mst && chmod 777 /var/log/mst
mkdir /var/log/nginx/mst && chown nginx:adm /var/log/nginx/mst && chmod 755 /var/log/nginx/mst
ln -s /vagrant/mst_server /var/www/mst
cat etc/.bash_profile > /root/.bash_profile
source /root/.bash_profile
mysql -u root -e "show databases" | grep MST || mysql -u root -e "create database MST"
doctrine orm:schema-tool:create
yum list installed gcc-c++ || yum install gcc-c++ -y
yum list installed libuuid-devel || yum install libuuid-devel -y
yum list installed libicu-devel || yum install libicu-devel -y
phpbrew ext | grep uuid | grep "*" || phpbrew ext install uuid
phpbrew ext | grep intl | grep "*" || phpbrew ext install intl
mkdir /var/local/mst && chown nginx:nginx /var/local/mst && chmod 700 /var/local/mst
cat /etc/nginx/sites-available/default.conf && mv /etc/nginx/sites-available/default.conf /etc/nginx/sites-available/default.conf.sample
cp etc/mst.conf /etc/nginx/sites-available/mst.conf
service nginx restart
