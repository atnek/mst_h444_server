<?php

namespace MST {

    use \Symfony\Component\HttpFoundation\Request,
        \Symfony\Component\HttpKernel\HttpKernelInterface;

    class App extends \Silex\Application
    {
        use Util\ComputedProperty;
        private static $instance;

        public static function app()
        {
            if (empty(self::$instance)) {
                self::$instance = new App();
            }
            return self::$instance;
        }

        public function forward($uri, $method = null)
        {
            if (!isset($method)) {
                $method = $this['request']->getMethod();
            }

            return $this->handle(
                    Request::create($uri, $method), HttpKernelInterface::SUB_REQUEST
            );
        }

        public function getDB()
        {
            return $this['db'];
        }

        public function getRedis()
        {
            return $this['predis'];
        }

        public function getSession()
        {
            return $this['session'];
        }

        public function getIsDebug()
        {
            return $this['debug'];
        }

        public function getLog()
        {
            return $this['log'];
        }

        public function getEM()
        {
            return $this['orm.em'];
        }
        
        public function getView()
        {
            return $this['twig'];
        }
    }

}