<?php

namespace MST\Auth {

    use MST\Auth\User,
        MST\Auth\Method\AuthenticationMethodInterface;

    class Authenticator
    {
        private $method;

        public function setMethod(AuthenticationMethodInterface $method)
        {
            $this->method = $method;
        }

        public function auth(User $user)
        {
            $this->method->auth($user);
        }

        public function register(User $user)
        {
            $this->method->register($user);
        }

    }

}