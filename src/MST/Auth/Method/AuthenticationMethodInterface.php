<?php

namespace MST\Auth\Method {

    use MST\Auth\User;

    interface AuthenticationMethodInterface
    {

        public function auth(User $user);

        public function register(User$user);
    }

}