<?php

namespace MST\Auth\Method {

    use MST\App,
        MST\Auth\User;

    class DBAuthentication implements AuthenticationMethodInterface
    {

        public function auth(User $user)
        {
            /* @var $db \Doctrine\DBAL\Connection */
            $db = App::app()->db;
            
            return true;
        }

        public function register(User $user)
        {
            $db = App::app()->db;
        }

    }

}