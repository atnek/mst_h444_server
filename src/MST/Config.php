<?php

namespace MST {

    class Config
    {
        /**
         * アプリケーションに関する設定
         * デバッグを有効にする
         */
        const DEBUG = true;

        /**
         * DBに関する設定
         * db.optionsに設定する
         * @var array
         */
        public static $DBParams = [
            'dbname' => 'MST',
            'user' => 'root',
            'password' => '',
            'host' => 'localhost',
            'driver' => 'pdo_mysql',
            'charset' => 'utf8mb4',
        ];

        /**
         * DBに関する設定
         * オートコミットを使用するか
         */
        const DB_UseAutoCommit = false;

        /**
         * DBに関する設定
         * 静的プレースホルダを使用するか
         */
        const DB_UseStaticPlaceHolder = true;

        /**
         * Redisに関する設定
         * Redisのホスト
         */
        const Redis_Host = 'tcp://127.0.0.1:6379';

        /**
         * 絵本データ(zipや画像)を保存するディレクトリ
         */
        const BOOK_DIR = '/var/local/mst/books/';
        const APP_ZIP_NAME = 'app.zip';
        const DISPLAY_ZIP_NAME = 'display.zip';

        public static $Controllers = [
            Controller\MSTApiController::class,
            Controller\MSTController::class,
        ];

    }

}