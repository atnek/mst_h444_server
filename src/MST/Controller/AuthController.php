<?php

namespace MST\Controller {

    use MST\Auth\Authenticator,
        MST\Auth\Method\DBAuthentication,
        Symfony\Component\HttpFoundation\Request;

    class AuthController extends BaseController
    {

        public function createUser(Request $request)
        {
            $authenticator = new Authenticator();
            $authenticator->setMethod(new DBAuthentication());
        }

    }

}