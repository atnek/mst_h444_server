<?php

namespace MST\Controller {

    use Symfony\Component\HttpFoundation\JsonResponse;

    abstract class BaseController
    {
        // trait
        use \MST\Util\ComputedProperty;

        /*
         * @var \Silex\Application
         */
        protected $app;

        public function __construct(\Silex\Application $app)
        {
            $this->app = $app;
        }

        protected function getSession()
        {
            return $this->app->session;
        }

        protected function getRepository($entity)
        {
            if (!is_string($entity)) {
                $entity = get_class($entity);
            }
            return $this->app->em->getRepository($entity);
        }

        protected function json($data = null, $status = 200, $headers = [])
        {
            return new JsonResponse($data, $status, $headers);
        }

        protected function stream($file)
        {
            if (file_exists($file)) {
                return $this->app->stream(function () use ($file) {
                        readfile($file);
                    });
            }
            return $this->badRequest('not exists file');
        }

        protected function file($file, $displayName = null, array $headers = [])
        {
            if (file_exists($file)) {
                $fileResponse = $this->app->sendFile($file, 200, $headers);
                if ($displayName != null) {
                    $fileResponse->setContentDisposition(\Symfony\Component\HttpFoundation\ResponseHeaderBag::DISPOSITION_ATTACHMENT, $displayName);
                }
                return $fileResponse;
            }
            $this->app->abort(404);
        }

        protected function ok($content)
        {
            return new \Symfony\Component\HttpFoundation\Response($content);
        }

        protected function badRequest($content, $status = 404)
        {
            return new \Symfony\Component\HttpFoundation\Response($content, $status);
        }

        protected function render($view, array $params = [], $response = null)
        {
            return $this->app->view->render($view, $params, $response);
        }

        protected function redirect($url)
        {
            return $this->app->redirect($url);
        }

        protected function form(array $names = [], $type = 'form', $data = null, array $options = [])
        {
            /* @var $form \Symfony\Component\Form\FormBuilderInterface */
            $form = $this->app['form.factory']->createBuilder($type, $data, $options);
            foreach ($names as $name => $option) {
                if (!isset($option)) {
                    $form->add($name);
                } else {
                    if (!is_array($option)) {
                        $option = [$option];
                    }
                    $form->add($name, ...$option);
                }
            }
            return $form->getForm();
        }

    }

}