<?php

namespace MST\Controller {

    use MST\Config,
        MST\Entity,
        Symfony\Component\HttpFoundation\Request;

    class MSTApiController extends BaseController
    {

        public function __construct(\Silex\Application $app)
        {
            parent::__construct($app);
        }

        public function index(Request $request)
        {
            $val = $this->session->get('session2');

            if (isset($val)) {
                $val++;
            } else {
                $val = 1;
            }

            $this->session->set('session2', $val);

            return $this->session->get('session2');
            // return phpinfo();
        }

        public function top(Request $request)
        {
            return phpinfo();
        }

        public function registerUser($user, $pass)
        {
            $encUser = $this->app['security.encoder_factory']->getEncoder($user);
            $encUser->encodePassword($pass);
        }

        public function bookList()
        {
            /* @var $repo Repository\BookRepository */
            $repo = $this->getRepository(Entity\Book::class);
            return $this->json($repo->findAll());
        }

        public function downloadAppResource($bookId)
        {
            static $headers = [
                'Content-Type' => 'application/zip',
                'Content-Description' => 'binary',
                'Expires' => '0',
                'Cache-Control' => 'private, no-store, no-cache, must-revalidate',
            ];
            return $this->file(Config::BOOK_DIR . $bookId . '/' . Config::APP_ZIP_NAME, 'book.zip', $headers);
        }

        public function downloadDisplayResource($bookId)
        {
            static $headers = [
                'Content-Type' => 'application/zip',
                'Content-Description' => 'binary',
                'Expires' => '0',
                'Cache-Control' => 'private, no-store, no-cache, must-revalidate',
            ];
            return $this->file(Config::BOOK_DIR . $bookId . '/' . Config::DISPLAY_ZIP_NAME, 'book.zip', $headers);
        }

    }

}
