<?php

namespace MST\Controller {

    use MST\Entity,
        MST\Repository,
        Symfony\Component\Validator\Constraints,
        Symfony\Component\HttpFoundation\Request;

    class MSTController extends BaseController
    {

        public function __construct(\Silex\Application $app)
        {
            parent::__construct($app);
        }

        public function login(Request $request)
        {
            $names = [
                'user' => 'text',
                'password' => 'password',
            ];
            /* @var $form \Symfony\Component\Form\Form */
            $form = $this->form($names);
            
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();
                // チェックしない
                if (true) {
                    return $this->app->get('/book');
                }
            }
            return $this->render('login.twig', ['form' => $form->createView()]);
        }

        public function book(Request $request)
        {
            $names = [
                'bookname' => ['text', ['constraints' => [new Constraints\NotBlank()]]],
                'upload_app' => ['file', ['constraints' => [new Constraints\NotBlank()]]],
                'upload_disp' => ['file', ['constraints' => [new Constraints\NotBlank()]]],
            ];
            
            /* @var $form \Symfony\Component\Form\Form */
            $form = $this->form($names);

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                /* @var $repo Repository\BookRepository */
                $repo = $this->getRepository(Entity\Book::class);
                $result = $repo->saveBook($data['bookname'], $data['upload_app'], $data['upload_disp']);
                if (!$result) {
                    return $this->badRequest('error zip');
                }
                return $this->ok('succeed zip');
            }
            return $this->render('upload.twig', ['form' => $form->createView()]);
        }

    }

}
