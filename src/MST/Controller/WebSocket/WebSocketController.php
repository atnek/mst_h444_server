<?php

namespace MST\Controller\WebSocket {

    use MST\Network\Rpc,
        MST\Network\WebSocket\WebSocketGroup,
        Ratchet\ConnectionInterface;

    class WebSocketController implements \Ratchet\MessageComponentInterface, \MST\Network\Rpc\RPCServerInterface
    {
        use \MST\Util\ComputedProperty;
        /**
         *
         * @var array
         */
        protected $groups;

        /**
         * @var Rpc\JsonRPC
         */
        private $rpc;

        public function __construct()
        {
            $this->groups = [];
            $this->rpc = new Rpc\JsonRPC($this);
            $this->rpc->server = $this;
        }

        public function onClose(ConnectionInterface $conn)
        {
            echo 'disconnected id: ' . $conn->resourceId . PHP_EOL;
            
            $removingIds = [];
            /* @var $group WebSocketGroup */
            foreach ($this->groups as $id => $group) {
                if ($group->clients->contains($conn)) {
                    $group->clients->detach($conn);    // クライアントを削除
                }
                if ($group->clients->count() < 1) {
                    $removingIds[] = $id;
                }
            }

            foreach ($removingIds as $id) {
                unset($this->groups[$id]);              // クライアントがいなくなったグループを削除
            }
        }

        public function onError(ConnectionInterface $conn, \Exception $e)
        {
            $conn->close();
        }

        /**
         * データの受信窓口
         * @param ConnectionInterface $from
         * @param type $msg
         */
        public function onMessage(ConnectionInterface $from, $msg)
        {
            echo 'onmessage id: ' . $from->resourceId . PHP_EOL;
            echo $msg . PHP_EOL;
            
            $this->rpc->request($from, $msg);
        }

        public function onOpen(ConnectionInterface $conn)
        {
            echo 'connected id: ' . $conn->resourceId . PHP_EOL;
        }

        /**
         * JSONRPC通信でエラーが起きた場合
         * @param object $request
         * @param ConnectionInterface $client
         * @param object $error
         */
        public function error($request, $client, $error)
        {
            $id = property_exists($request, 'id') ? $request->id : '';
            $client->send($this->rpc->badRequest($error, $id));
        }

        /**
         * 通信開始直後に必ず実行
         * @param ConnectionInterface $client
         * @param int $id コネクションID
         * @param string $userId ユーザID
         * @param HardWareModel $hardWare
         */
        public function handShake($client, $id, $userId, $hardWare)
        {
            if (!property_exists($hardWare, 'kind')) {              // "kind"パラメータ存在チェック
                $error = new Rpc\Error\JsonRPCInvalidParamsError();
                $error->message .= ': require "kind" .';
                $response = $this->rpc->badRequest($error, $id);
                $client->send($response);
                return;
            } else {
                static $kinds = ['app', 'display'];
                if (!in_array($hardWare->kind, $kinds)) {           // 定義していないkindのチェック
                    $error = new Rpc\Error\JsonRPCInvalidParamsError();
                    $error->message .= ': unknown kind .';
                    $response = $this->rpc->badRequest($error, $id);
                    $client->send($response);
                    return;
                }
            }

            if (!array_key_exists($userId, $this->groups)) {        // このユーザのグループが既に存在しているか
                switch ($hardWare->kind) {
                    case 'app':     // 新規グループ作成
                        $this->groups[$userId] = new WebSocketGroup();
                        break;
                    case 'display': // グループがない
                        $error = new Rpc\Error\JsonRPCInternalError();
                        $error->message .= ': not found group. please create a group by app.';
                        $response = $this->rpc->badRequest($error, $id);
                        $client->send($response);
                        return;
                }
            }

            $this->groups[$userId]->clients->attach($client);
            echo 'userid:' . $userId . PHP_EOL;
            $response = $this->rpc->ok(['messageId' => 1, 'message' => 'added']);
            $client->send($response);
        }

        public function takeOpenedBook($client, $id)
        {
            $group = $this->find($client);

            $response = $this->rpc->ok(['messageId' => 5]);
            $group->notify($response);
        }

        // 絵本を開いた際の同期
        public function openBook($client, $id, $bookId)
        {
            $group = $this->find($client);

            $response = $this->rpc->ok(['messageId' => 3, 'bookId' => $bookId]);
            $group->notify($response);
        }

        // 現在のページの同期
        public function pageSync($client, $id, $page)
        {
            $group = $this->find($client);

            $response = $this->rpc->ok(['messageId' => 2, 'page' => $page]);
            $group->notify($response);
        }

        /**
         * ボイス中継
         * @param ConnectionInterface $client
         * @param int $id
         * @param int $voiceNo
         * @param string $to
         */
        public function relayVoice($client, $id, $voiceNo, $to = 'display')
        {
            $group = $this->find($client);

            if (!is_numeric($voiceNo)) {
                // ボイス番号エラー
                $error = new Rpc\Error\JsonRPCInvalidParamsError();
                $error->message .= ': invalid voice number. required number';
                $response = $this->rpc->badRequest($error, $id);
            } else if ($to !== 'app' && $to !== 'display') {
                // 送り先エラー
                $error = new Rpc\Error\JsonRPCInvalidParamsError();
                $error->message .= ': invalid destination. "app" or "display"';
                $response = $this->rpc->badRequest($error, $id);
            } else {
                // 正常
                $response = $this->rpc->ok(['messageId' => 4, 'no' => $voiceNo, 'to' => $to]);
            }
            $group->notify($response);
        }

        public function test($client, $id, $a)
        {
            $result = 'server echo: ' . $a;
            $response = $this->rpc->ok($result, $id);
            $client->send($response);
        }

        private function find(ConnectionInterface $client)
        {
            foreach ($this->groups as $group) {
                if ($group->clients->contains($client)) {
                    return $group;
                }
            }
            return null;
        }

    }

}
