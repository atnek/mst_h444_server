<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace MST\Entity {

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="MST\Repository\AccountRepository")
     * @ORM\Table(name="account", options={"charset" = "utf8mb4", "collate"="utf8mb4_general_ci"})
     * @ORM\HasLifecycleCallbacks
     */
    class Account
    {
        use \MST\Util\ComputedProperty,
            \MST\Util\CommonColumn;

        /**
         * @ORM\Column(type="string", name="user_id", unique=true, length=64)
         * @var string
         */
        public $userId;

        /**
         * @ORM\Column(type="string", name="password")
         * @var string
         */
        public $password;

        /**
         * @ORM\Column(type="datetime", name="last_login", columnDefinition="datetime default now()")
         * @var string
         */
        protected $_lastLogin;

        public function getLastLogin()
        {
            return $this->_lastLogin;
        }
    }

}
