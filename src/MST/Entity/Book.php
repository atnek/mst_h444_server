<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace MST\Entity {

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="MST\Repository\BookRepository")
     * @ORM\Table(name="book", options={"charset" = "utf8mb4", "collate"="utf8mb4_general_ci"})
     * @ORM\HasLifecycleCallbacks
     */
    class Book
    {
        use \MST\Util\ComputedProperty,
            \MST\Util\CommonColumn;
        /**
         * @ORM\Column(type="string", name="book_id", unique=true, length=36)
         * @var string
         */
        public $bookId;

        /**
         * @ORM\Column(type="string")
         * @var string 
         */
        public $name;

        /**
         * @ORM\PrePersist
         */
        public function beforePersist()
        {
            $this->bookId = uuid_create(UUID_TYPE_RANDOM);
        }
    }

}