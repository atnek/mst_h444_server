<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace MST\Entity {

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="MST\Repository\DeviceRepository")
     * @ORM\Table(name="device", options={"charset" = "utf8mb4", "collate"="utf8mb4_general_ci"})
     * @ORM\HasLifecycleCallbacks
     */
    class Device
    {
        use \MST\Util\ComputedProperty,
            \MST\Util\CommonColumn;
        /**
         * @ORM\Column(type="string", name="device_id", unique=true, length=36)
         * @var string
         */
        protected $_deviceId;

        public function getDeviceId()
        {
            return $this->_deviceId;
        }

        /**
         * @ORM\Column(type="string", length=50)
         * @var string
         */
        public $name;

        /**
         * @ORM\PrePersist
         */
        public function beforePersist()
        {
            $this->_bookId = uuid_create(UUID_TYPE_RANDOM);
        }

    }

}
