<?php

namespace MST\Entity {

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="MST\Repository\OwnBookRepository")
     * @ORM\Table(name="own_book", options={"charset" = "utf8mb4", "collate"="utf8mb4_general_ci"})
     */
    class OwnBook
    {
        use \MST\Util\ComputedProperty,
            \MST\Util\CommonColumn;

        /**
         * @ORM\ManyToOne(targetEntity="User")
         * @ORM\JoinColumn(name="user_serial", referencedColumnName="serial")
         * @var int
         */
        public $user;

        /**
         * @ORM\ManyToOne(targetEntity="Book")
         * @ORM\JoinColumn(name="book_serial", referencedColumnName="serial")
         * @var int
         */
        public $book;

    }

}