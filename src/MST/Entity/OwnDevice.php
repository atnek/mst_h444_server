<?php

namespace MST\Entity {

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="MST\Repository\OwnDeviceRepository")
     * @ORM\Table(name="own_device", options={"charset" = "utf8mb4", "collate"="utf8mb4_general_ci"})
     */
    class OwnDevice
    {
        use \MST\Util\ComputedProperty,
            \MST\Util\CommonColumn;

        /**
         * @ORM\ManyToOne(targetEntity="User")
         * @ORM\JoinColumn(name="user_serial", referencedColumnName="serial")
         * @var int
         */
        public $user;

        /**
         * @ORM\ManyToOne(targetEntity="Device")
         * @ORM\JoinColumn(name="device_serial", referencedColumnName="serial")
         * @var int
         */
        public $device;

    }

}