<?php

namespace MST\Entity {

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="MST\Repository\UserRepository")
     * @ORM\Table(name="user", options={"charset" = "utf8mb4", "collate"="utf8mb4_general_ci"})
     */
    class User
    {
        use \MST\Util\ComputedProperty,
            \MST\Util\CommonColumn;

        /**
         * @ORM\Column(type="string")
         * @var string
         */
        public $name;

        /**
         * @ORM\OneToOne(targetEntity="Account")
         * @ORM\JoinColumn(name="serial", referencedColumnName="serial")
         * @var int
         */
        public $account;

    }

}
