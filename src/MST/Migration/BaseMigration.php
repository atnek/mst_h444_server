<?php

abstract class BaseMigration extends Migration
{

    abstract protected function createUpSQL();

    abstract protected function createDownSQL();

    public function up()
    {
        $this->exec($this->createUpSQL());
    }

    public function down()
    {
        $this->exec($this->createDownSQL());
    }

    private function exec($sql)
    {
        $container = $this->getContainer();

        /* @var $conn Doctrine\DBAL\Connection */
        $conn = $container['db'];

        if (is_array($sql)) {
            foreach ($sql as $s) {
                $conn->exec($s);
            }
        } else {
            if (!empty($sql)) {
                $conn->exec($sql);
            }
        }
    }

}
