<?php

class FirstMigration extends BaseMigration
{

    protected function createDownSQL()
    {
        return [
            'drop table if exists OWN_BOOK cascade',
            'drop table if exists BOOK cascade',
            'drop table if exists ACCOUNT cascade',
            'drop table if exists USER cascade',
        ];
    }

    protected function createUpSQL()
    {
        return [
            'create table OWN_BOOK (
                ID INT not null comment "所持絵本ID"
                , USER_ID INT not null comment "ユーザID"
                , BOOK_ID INT not null comment "絵本ID"
                , CREATE_TIME DATETIME not null comment "作成日時"
                , UPDATE_TIME TIMESTAMP not null comment "更新日時"
                , constraint OWN_BOOK_PKC primary key (ID)
              ) comment "所持絵本テーブル"',
            'create table BOOK (
                ID INT not null comment "絵本ID"
                , NAME VARCHAR(100) comment "絵本名"
                , KEY CHAR(36) comment "アクセスキー"
                , PAGE_COUNT INT comment "ページ数"
                , DATA_LENGTH INT comment "データ容量"
                , CREATE_TIME DATETIME not null comment "作成日時"
                , UPDATE_TIME TIMESTAMP not null comment "更新日時"
                , constraint BOOK_PKC primary key (ID)
              ) comment "絵本テーブル"',
            'create table ACCOUNT (
                ID INT not null comment "ユーザID"
                , PASSWORD CHAR(128) not null comment "パスワード"
                , CREATE_TIME DATETIME not null comment "作成日時"
                , UPDATE_TIME TIMESTAMP not null comment "更新日時"
                , constraint ACCOUNT_PKC primary key (ID)
              ) comment "アカウントテーブル"',
            'create table USER (
                ID INT not null comment "ユーザID"
                , NAME VARCHAR(50) not null comment "ユーザ名"
                , MAIL VARCHAR(200) not null comment "メールアドレス"
                , CREATE_TIME DATETIME not null comment "作成日時"
                , UPDATE_TIME TIMESTAMP not null comment "更新日時"
                , constraint USER_PKC primary key (ID)
              ) comment "ユーザテーブル"',
            'alter table OWN_BOOK
                add foreign key (BOOK_ID) references BOOK(ID)
                on delete cascade',
            'alter table OWN_BOOK
                add foreign key (USER_ID) references USER(ID)
                on delete cascade',
            'alter table ACCOUNT
                add foreign key (ID) references USER(ID)
                on delete cascade',
        ];
    }

}
