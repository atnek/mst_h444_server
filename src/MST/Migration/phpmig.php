<?php

require_once __DIR__ . '/../../../vendor/autoload.php';
require_once __DIR__ . '/BaseMigration.php';

use MST\Config,
    Doctrine\DBAL\Configuration,
    Doctrine\DBAL\DriverManager,
    Phpmig\Adapter\Doctrine\DBAL;

$container = new Pimple();

$container['db'] = $container->share(function () {
    return DriverManager::getConnection(
            Config::$DBParams,
            //TestConfig::$DBParams,
            new Configuration([
            \PDO::ATTR_AUTOCOMMIT => Config::DB_UseAutoCommit,
            \PDO::ATTR_EMULATE_PREPARES => !Config::DB_UseStaticPlaceHolder,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            ])
    );
});

// replace this with a better Phpmig\Adapter\AdapterInterface
$container['phpmig.adapter'] = $container->share(function () use($container) {
    return new DBAL($container['db'], 'mst_migrations');
});

$container['phpmig.migrations_path'] = __DIR__ . DIRECTORY_SEPARATOR . 'migrations';

// You can also provide an array of migration files
/*
  $container['phpmig.migrations'] = array_merge(
  glob('migrations/v1/*.php')
  //glob('migrations/v2/*.php')
  );
 */

return $container;
