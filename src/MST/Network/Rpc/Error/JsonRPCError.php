<?php

namespace MST\Network\Rpc\Error {

    abstract class JsonRPCError
    {
        public $code;
        public $message;
        public $data;

    }

}