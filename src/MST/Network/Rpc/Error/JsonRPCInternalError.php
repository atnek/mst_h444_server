<?php

namespace MST\Network\Rpc\Error {

    class JsonRPCInternalError extends JsonRPCError
    {

        public function __construct()
        {
            $this->code = -32603;
            $this->message = 'Internal error';
        }

    }

}