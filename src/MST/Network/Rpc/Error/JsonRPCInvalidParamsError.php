<?php

namespace MST\Network\Rpc\Error {

    class JsonRPCInvalidParamsError extends JsonRPCError
    {

        public function __construct()
        {
            $this->code = -32602;
            $this->message = 'Invalid Params';
        }

    }

}