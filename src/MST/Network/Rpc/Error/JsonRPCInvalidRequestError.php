<?php

namespace MST\Network\Rpc\Error {

    class JsonRPCInvalidRequestError extends JsonRPCError
    {

        public function __construct($message = '')
        {
            $this->code = -32600;
            $this->message = 'Invalid Request: ' . $message;
        }

    }

}