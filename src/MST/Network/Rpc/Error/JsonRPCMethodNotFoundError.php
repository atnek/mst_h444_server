<?php

namespace MST\Network\Rpc\Error {

    class JsonRPCMethodNotFoundError extends JsonRPCError
    {

        public function __construct($method)
        {
            $this->code = -32601;
            $this->message = 'Method not found: ' . $method;
        }

    }

}