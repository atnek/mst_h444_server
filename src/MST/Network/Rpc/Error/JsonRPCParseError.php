<?php

namespace MST\Network\Rpc\Error {

    class JsonRPCParseError extends JsonRPCError
    {

        public function __construct()
        {
            $this->code = -32700;
            $this->message = 'Parse error';
        }

    }

}