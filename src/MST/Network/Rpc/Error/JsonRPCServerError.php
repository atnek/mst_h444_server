<?php

namespace MST\Network\Rpc\Error {

    class JsonRPCServerError extends JsonRPCError
    {
        const CODE_MIN = -32000;
        const CODE_MAX = -32099;

        public function __construct($code, $message)
        {
            if (!$this->isValidCode($code)) {
                // TODO out of code error
            }

            $this->code = $code;
            $this->message = $message;
        }

        private function isValidCode($code)
        {
            return self::CODE_MAX <= $code && $code <= self::CODE_MIN;
        }

    }

}