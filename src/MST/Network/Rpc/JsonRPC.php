<?php

namespace MST\Network\Rpc {

    class JsonRPC extends RPC
    {

        protected function checkFormat($req)
        {
            // JSONかどうか
            if (empty($req)) {
                // JSONの形式ではない
                return new Error\JsonRPCParseError();
            }

            static $reqMember = [
                'jsonrpc' => ['required' => true],
                'method' => ['required' => true],
                'params' => ['required' => false],
                'id' => ['required' => false],
            ];

            foreach ($reqMember as $name => $attr) {
                if (!$attr['required']) {
                    // 必須でないならチェックしない
                    continue;
                }

                if (!property_exists($req, $name)) {
                    // JsonRPCの形式ではない
                    return new Error\JsonRPCInvalidRequestError($name);
                }
            }

            if ($req->jsonrpc !== '2.0') {
                // JsonRPCバージョンエラー
                return new Error\JsonRPCInvalidRequestError("require rpc version 2.0. your version " . $req->jsonrpc);
            }

            return null;
        }

        protected function decode($messege)
        {
            return json_decode($messege);
        }

        protected function notFoundMethodError($method)
        {
            return new Error\JsonRPCMethodNotFoundError($method);
        }

        public function toResponse($result = null, $id = null, Error\JsonRPCError $error = null)
        {
            $response = ['jsonrpc' => '2.0', 'result' => $result, 'error' => $error];
            if (isset($id)) {
                $response['id'] = $id;
            }

            return json_encode($response);
        }

        public function ok($result, $id = null)
        {
            return $this->toResponse($result, $id);
        }

        public function badRequest(Error\JsonRPCError $error, $id)
        {
            return $this->toResponse(null, $id, $error);
        }

    }

}
