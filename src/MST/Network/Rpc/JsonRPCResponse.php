<?php

namespace MST\Network\Rpc {

    class JsonRPCResponse extends RPCResponse
    {
        private $id;

        public function __construct($obj, $id = null)
        {
            $this->id = $id;
            parent::__construct($obj);
        }

        protected function format($method, $result = null, $isError = false)
        {

            $response = ['jsonrpc' => '2.0'];

            if ($isError) {
                $response['method'] = $method;
                if ($result !== null) {
                    $response['result'] = $result;
                }
            } else {
                $response['error'] = $result;
            }

            if ($this->id !== null) {
                $response['id'] = $this->id;
            }

            return json_encode($response);
        }

    }

}