<?php

namespace MST\Network\Rpc {

    abstract class RPC
    {
        protected $obj;

        /**
         *
         * @var RPCServerInterface
         */
        public $server;

        public function __construct($class)
        {
            $this->obj = $class;
        }

        public function request($client, $message, $useCheck = true)
        {
            $request = $this->decode($message);
            if ($useCheck) {
                $error = $this->checkFormat($request);

                // エラー
                if ($error !== null) {
                    $this->server->error($request, $client, $error);
                    return;
                }
            }

            $method = $request->method;
            $params = $request->params ? : [];
            $id = $request->id;

            if (!is_array($params)) {
                $params = [$params];
            }

            try {
                if (method_exists($this->obj, $method)) {
                    call_user_func_array([$this->obj, $method], array_merge([$client, $id], $params));
                } else {
                    
                }
            } catch (RPCException $e) {
                $this->server->error($client, $e->getError());
            }
        }

        protected abstract function decode($messege);

        protected abstract function checkFormat($req);

        protected abstract function notFoundMethodError($method);
    }

}
