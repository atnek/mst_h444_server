<?php

namespace MST\Network\Rpc {

    abstract class RPCException extends \Exception
    {
        protected $error;

        public function getError()
        {
            return $this->error;
        }

        public function __construct($message, $error, $code = null, $previous = null)
        {
            $this->error = $error;
            parent::__construct($message, $code, $previous);
        }

    }

}