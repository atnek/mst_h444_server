<?php

namespace MST\Network\Rpc {

    abstract class RPCResponse
    {
        use \MST\Util\ComputedProperty;
        protected $response;

        public function getResponse()
        {
            return $this->response;
        }

        /**
         * RPCResponseの形式に変換する
         * @param string $method
         * @param mixed $result
         * @param bool $isError
         */
        protected abstract function format($method, $result = null, $isError = false);

        public function __construct($obj)
        {
            $this->response = $this->format($obj);
        }

    }

}