<?php

namespace MST\Network\Rpc {

    interface RPCServerInterface
    {

        function error($request, $client, $error);
    }

}