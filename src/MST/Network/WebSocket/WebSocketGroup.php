<?php

namespace MST\Network\WebSocket {

    class WebSocketGroup
    {
        /**
         * @var \SplObjectStorage
         */
        public $clients;

        public function __construct()
        {
            $this->clients = new \SplObjectStorage();
        }

        public function notify($response, callable $canSend = null)
        {
            $canSend = $canSend ? : function($client) {
                return true;
            };
            foreach ($this->clients as $client) {
                if ($canSend($client)) {
                    $client->send($response);
                }
            }
        }

        public function notifyOthers($response, $me)
        {
            $this->notify($response, function ($client) use($me) {
                return $client->resourceId !== $me->resourceId;
            });
        }

    }

}