<?php

namespace MST\Provider {

    use Symfony\Component\Security\Core\User\User,
        Symfony\Component\Security\Core\Exception\UsernameNotFoundException,
        Symfony\Component\Security\Core\User\UserProviderInterface;

    class AccountProvider implements UserProviderInterface
    {

        public function loadUserByUsername($username)
        {
            static $account = [
                'root' => 'SSSlwp94DHfTHNZ4rDlVeTHqB2CXJyoxRP0DfNWQUArxi4+BG7oSkmvsIujycf5SzNY1VEiu08fZd3WO7TqyBA==',
            ];

            if (array_key_exists($username, $account)) {
                $password = $account[$username];
                return new User($username, $password);
            }
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }

        public function refreshUser(\Symfony\Component\Security\Core\User\UserInterface $user)
        {
            if (!$user instanceof User) {
                throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
            }

            return $this->loadUserByUsername($user->getUsername());
        }

        function supportsClass($class)
        {
            return $class === User::class;
        }

    }

}