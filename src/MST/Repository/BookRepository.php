<?php

namespace MST\Repository {

    use Doctrine\ORM\EntityRepository,
        MST\Config,
        MST\Entity\Book,
        Symfony\Component\HttpFoundation\File\UploadedFile;

    class BookRepository extends EntityRepository
    {

        public function saveBook($bookName, UploadedFile $appResource, UploadedFile $dispayResource)
        {
            $em = $this->_em;
            $em->beginTransaction();

            try {
                // BOOKIDを生成
                $book = $this->createBook($bookName);

                $bookDir = Config::BOOK_DIR . $book->bookId;

                $appResource->move($bookDir, Config::APP_ZIP_NAME);
                $dispayResource->move($bookDir, Config::DISPLAY_ZIP_NAME);

                // 展開
                $imageDir = $bookDir . '/pages';
                if (!file_exists($imageDir)) {
                    mkdir($imageDir, 0744, true);
                }

                $zip = new \ZipArchive();
                $isOpened = $zip->open($bookDir . '/' . Config::APP_ZIP_NAME);

                if ($isOpened) {
                    $zip->extractTo($imageDir . '/');
                    $zip->close();

                    $em->commit();
                    return true;
                }
            } catch (Exception $ex) {
                // TODO logging
            }

            $this->rollback();
            return false;
        }

        private function createBook($bookName)
        {
            $book = new Book();
            $book->name = $bookName;
            $this->_em->persist($book);
            $this->_em->flush();

            return $book;
        }

    }

}