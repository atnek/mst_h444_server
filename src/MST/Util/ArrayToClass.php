<?php

namespace MST\Util {

    trait ArrayToClass
    {

        public function fromArray(array $array)
        {
            $self = new self;

            foreach ($array as $key => $value) {
                if (property_exists($self, $key)) {
                    $self->$key = $value;
                }
            }

            return $self;
        }

    }

}