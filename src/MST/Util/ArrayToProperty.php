<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace MST\Util {

    trait ArrayToProperty
    {

        public function import(array $datas)
        {
            $vars = get_object_vars($this);
            $classname = $this->getClassName();

            foreach ($vars as $var) {
                $prop = $datas[$classname . '_' . $var];
                if (isset($prop)) {
                    $this->$var = $prop;
                }
            }
        }

        public function getClassName()
        {
            $object = $this;
            if (!is_object($object) && !is_string($object)) {
                return false;
            }

            $class = explode('\\', (is_string($object) ? $object : get_class($object)));
            return $class[count($class) - 1];
        }

    }

}
