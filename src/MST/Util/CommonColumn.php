<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace MST\Util {

    /**
     * Entityの共通項目
     * ・serial
     * ・create_date
     * ・update_date
     */
    trait CommonColumn
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer", name="serial")
         * @ORM\GeneratedValue(strategy="AUTO")
         * @var int
         */
        protected $_serial;

        public function getSerial()
        {
            return $this->_serial;
        }

        /**
         * @ORM\Column(type="datetime", name="create_date", columnDefinition="datetime not null default now()")
         * @var string
         */
        protected $_createDate;

        public function getCreateDate()
        {
            return $this->_createDate;
        }

        /**
         * @ORM\Column(type="datetime", name="update_date")
         * @var string 
         */
        protected $_updateDate;
        
        public function getUpdateDate()
        {
            return $this->_updateDate;
        }
        
        /**
         * @ORM\PrePersist
         */
        public function setCreateDate()
        {
            $date = new \DateTime("now");
            $this->_createDate = $date;
            $this->_updateDate = $date;
        }
        
        /**
         * @ORM\PreUpdate
         */
        public function setUpdateDate()
        {
            $this->_updateDate = new \DateTime("now");
        }
    }

}
