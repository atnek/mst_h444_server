<?php

namespace MST\Util {

    trait ComputedProperty
    {

        public function __get($name)
        {
            $getter = 'get' . $name;
            if (method_exists($this, $getter)) {
                return $this->$getter();
            }
        }

        public function __set($name, $value)
        {
            $setter = 'set' . $name;
            if (method_exists($this, $setter)) {
                return $this->$setter($value);
            }
        }

        public function __unset($name)
        {
            $setter = 'set' . $name;
            if (method_exists($this, $setter)) {
                return $this->$setter(null);
            }
        }

    }

}